package di

import (
	"errors"
)

var (
	ErrConstructorNotFunction = errors.New("unsupported type for constructor")
	ErrMismatchedTypes        = errors.New("expected and given types are different")
	ErrNotWired               = errors.New("missed wired constructor")
	ErrKeyNotFound            = errors.New("cannot find instance by key")
	ErrCircularDependencies   = errors.New("circular dependencies")
	ErrCloseFailed            = errors.New("fail to stop a service")
	ErrTooMuchTargets         = errors.New("only one target is supported")
	ErrTagsNoSlice            = errors.New("cannot assign tagged items to non-slice")
)
