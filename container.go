package di

import (
	"fmt"
	"io"
	"reflect"
)

func NewContainer() *container {
	return &container{
		entities: make(map[Address]*entity),
		graph:    make(map[string]*entity),
		tags:     make(map[any][]*tag),
	}
}

type container struct {
	entities map[Address]*entity
	graph    map[string]*entity
	tags     map[any][]*tag
}

func (c *container) Wire(constructor any, iface *Type, options ...wireOption) error {
	opts := make([]optionHandler, 0, len(options)+1)
	for _, o := range options {
		opts = append(opts, o.(option).handler())
	}
	opts = append(opts, aliasOf(iface).handler())

	_, err := c.wire(constructor, opts)
	return err
}

func (c *container) Define(constructor any, options ...defineOption) (Address, error) {
	opts := make([]optionHandler, 0, len(options))
	for _, o := range options {
		opts = append(opts, o.(option).handler())
	}

	return c.wire(constructor, opts)
}

func (c *container) Tags(label any) tagsHandler {
	return func() (e []*entity, err error) {
		if tags, found := c.tags[label]; found {
			for _, t := range tags {
				a := c.entities[t.address]
				if err = c.instantiate(a, t.iface); err != nil {
					return nil, err
				}
				e = append(e, a)
			}
		}
		return e, nil
	}
}

func (c *container) New(iface *Type, options ...newOption) (any, func() error, error) {
	r := &request{}
	var withCloser bool
	for _, o := range options {
		if _, ok := o.(*closerOption); ok {
			withCloser = true
		}
		if err := o.(option).handler()(r); err != nil {
			return nil, nil, err
		}
	}
	if len(r.targets) > 1 {
		return nil, nil, ErrTooMuchTargets
	}

	path := append([]*Type{iface}, r.targets...)
	e, err := c.resolve(path...)
	if err != nil {
		return nil, nil, err
	}
	err = c.instantiate(e, path...)
	if err != nil {
		return nil, nil, err
	}
	var closer func() error
	if withCloser {
		done := map[*entity]bool{}
		deps := append([]*entity{e}, c.liner(e)...)
		for idx := len(deps) - 1; idx >= 0; idx-- {
			if _, closed := done[deps[idx]]; closed {
				deps[idx] = nil
			}
			done[deps[idx]] = true
		}

		closer = c.closer(deps)
	}
	return e._i.Interface(), closer, err
}

func (c *container) wire(constructor any, options []optionHandler) (Address, error) {
	r := &request{
		constructor: constructor,
	}
	for _, opt := range options {
		if err := opt(r); err != nil {
			return "", err
		}
	}

	var e *entity
	var key Address
	if v, ok := r.constructor.(Address); ok {
		if e, ok = c.entities[v]; !ok {
			return "", fmt.Errorf("%w: '%s'", ErrKeyNotFound, r.constructor)
		}
		key = v
	} else if r.f().Kind() != reflect.Func {
		return "", ErrConstructorNotFunction
	} else {
		e = r.entity()
		if e._f.NumOut() > 0 && !e._f.Out(0).ConvertibleTo(reflect.TypeFor[error]()) {
			key = Address(fmt.Sprintf("%s:%p", e._f.Out(0).Deref().Name(), e))
			c.entities[key] = e
		}
	}

	if len(r.targets) == 0 {
		r.targets = append(r.targets, nil)
	}
	for _, alias := range r.aliases {
		for _, target := range r.targets {
			link := encrypt(alias.Name(), target.Name())
			if _, found := c.graph[link]; !found || !r.fallback {
				c.graph[link] = e
			}
		TAG:
			for _, label := range r.tags {
				for _, t := range c.tags[label] {
					if t.address == key {
						continue TAG
					}
				}
				c.tags[label] = append(c.tags[label], &tag{key, alias})
			}
		}
	}

	return key, nil
}

func (c *container) resolve(path ...*Type) (*entity, error) {
	if len(path) == 0 {
		return nil, fmt.Errorf("empty path")
	}

	if len(path) > 1 {
		hash := encrypt(path[0].Name(), path[1].Name())
		if e, found := c.graph[hash]; found {
			return e, nil
		}
	}

	hash := encrypt(path[0].Name(), "")
	if e, found := c.graph[hash]; found {
		return e, nil
	}

	return nil, fmt.Errorf("%w: '%s'", ErrNotWired, path[0].Name())
}

func (c *container) instantiate(e *entity, need ...*Type) error {
	if e._i != nil {
		return nil
	}
	var args []reflect.Value
	for idx := 0; idx < e._f.NumIn(); idx++ {
		field := e._f.In(idx)
		if arg, ok := e.defaults[idx]; ok {
			switch f := arg.(type) {
			case Address:
				a, ok := c.entities[f]
				if !ok {
					return fmt.Errorf("%w: '%s'", ErrKeyNotFound, f)
				}
				if err := c.instantiate(a, field); err != nil {
					return err
				}
				args = append(args, *a._i)
				e.graph = append(e.graph, a)
			case tagsHandler:
				if field.Kind() != reflect.Slice || field.Elem().Kind() != reflect.Interface {
					return ErrTagsNoSlice
				}
				tags, err := f()
				if err != nil {
					return err
				}

				a := reflect.New(field.Type).Elem()
				for _, t := range tags {
					if !t._i.CanConvert(a.Type().Elem()) {
						return fmt.Errorf("%w: '%s', '%s'", ErrMismatchedTypes, typeOf(a.Type().Elem()).Name(), typeOf(t._i.Type()).Deref().Name())
					}
					a.Set(reflect.Append(a, *t._i))
					e.graph = append(e.graph, t)
				}
				args = append(args, a)
			default:
				value := reflect.ValueOf(arg)
				if value.Kind() == reflect.Invalid {
					value = reflect.New(field.Type).Elem()
				}
				if value.Kind() == reflect.Func {
					v := value.Call([]reflect.Value{})
					value = v[0]
				}
				if field.Kind() != reflect.Interface && field.Kind() != value.Kind() {
					return fmt.Errorf("%w: '%s', '%s'", ErrMismatchedTypes, field.Name(), typeOf(value.Type()).Name())
				}
				if field.Kind() == reflect.Interface && !value.CanConvert(field.Type) {
					return fmt.Errorf("%w: '%s', '%s'", ErrMismatchedTypes, field.Name(), typeOf(value.Type()).Name())
				}
				args = append(args, value)
			}
		} else if e._f.In(idx).Kind() == reflect.Interface {
			for _, dep := range need {
				if dep.Name() == field.Name() {
					return fmt.Errorf("%w: '%s', '%s'", ErrCircularDependencies, field.Name(), dep.Name())
				}
			}
			fieldPath := append([]*Type{field}, need...)
			a, err := c.resolve(fieldPath...)
			if err != nil {
				return err
			}
			if err = c.instantiate(a, fieldPath...); err != nil {
				return err
			}
			args = append(args, *a._i)
			e.graph = append(e.graph, a)
		} else {
			args = append(args, reflect.New(field.Type).Elem())
		}
	}

	if e._f.IsVariadic() {
		variadic := args[len(args)-1]
		args = args[:len(args)-1]
		for vIdx := 0; vIdx < variadic.Len(); vIdx++ {
			args = append(args, variadic.Index(vIdx))
		}
	}

	value := reflect.New(need[0].Type).Elem()
	result := reflect.ValueOf(e.constructor).Call(args)
	if returns := e._f.NumOut(); returns > 0 {
		var hasError bool

		if e._f.Out(returns - 1).ConvertibleTo(reflect.TypeFor[error]()) {
			if err := result[returns-1].Interface(); err != nil {
				return err.(error)
			}
			hasError = true
		}
		if !hasError || returns > 1 {
			value = result[0]
		}
	}
	e.Initialize(&value)
	return nil
}

func (c *container) liner(e *entity) (result []*entity) {
	result = append(result, e.graph...)
	for _, dep := range e.graph {
		result = append(result, c.liner(dep)...)
	}
	return
}

func (c *container) closer(deps []*entity) func() error {
	return func() error {
		for idx := range deps {
			if deps[idx] == nil {
				continue
			}
			if closable, ok := deps[idx]._i.Interface().(io.Closer); ok {
				err := func() (err error) {
					defer func() {
						if err == nil {
							if r := recover(); r != nil {
								var ok bool
								if err, ok = r.(error); !ok {
									err = fmt.Errorf("%v", r)
								}
							}
						}
					}()

					return closable.Close()
				}()
				if err != nil {
					return fmt.Errorf("%w: %s", ErrCloseFailed, deps[idx]._i.String())
				}
			}
		}
		return nil
	}
}
