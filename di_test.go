package di_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gorib/di"
)

func TestInit(t *testing.T) {
	c := di.NewContainer()
	assert.NotNil(t, c)
}

func TestDi(t *testing.T) {
	t.Parallel()

	t.Run("NotFunction", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(7)
		assert.ErrorIs(t, err, di.ErrConstructorNotFunction)
	})

	t.Run("Success", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		addr, err := di.Define(NewService)
		assert.NoError(t, err)
		assert.NotNil(t, addr)
	})

	t.Run("WrongAddress", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(di.Address("some key"))
		assert.ErrorIs(t, err, di.ErrKeyNotFound)
	})

	t.Run("ByCorrectAddress", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		addr, err := di.Define(NewService)
		assert.NoError(t, err)
		assert.NotNil(t, addr)
		err = di.Wire[Service](addr)
		assert.NoError(t, err)
	})

	t.Run("New", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.NoError(t, err)
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &service{}, srv)
	})

	t.Run("Chain", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.NoError(t, err)
		err = di.Wire[ServiceChain](NewServiceChain)
		assert.NoError(t, err)
		srv, err := di.New[ServiceChain]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceChain{}, srv)
		child := srv.(*serviceChain).Service()
		assert.NotNil(t, child)
		assert.IsType(t, &service{}, child)
	})

	t.Run("ResolveFail", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[ServiceChain](NewServiceChain)
		assert.NoError(t, err)
		_, err = di.New[ServiceChain]()
		assert.ErrorIs(t, err, di.ErrNotWired)
	})

	t.Run("ResolveChainFail", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.New[Service]()
		assert.ErrorIs(t, err, di.ErrNotWired)
	})

	t.Run("Aliases", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		addr, err := di.Define(NewService, di.Alias[ServiceAlias]())
		assert.NoError(t, err)
		assert.NotNil(t, addr)
		assert.IsType(t, di.Address(""), addr)
		err = di.Wire[Service](addr)
		assert.NoError(t, err)
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &service{}, srv)
		srvAlias, err := di.New[ServiceAlias]()
		assert.NoError(t, err)
		assert.NotNil(t, srvAlias)
		assert.IsType(t, &service{}, srvAlias)
		assert.Equal(t, fmt.Sprintf("%p", srv), fmt.Sprintf("%p", srvAlias))
	})

	t.Run("WiredAliases", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.NoError(t, err)
		err = di.Wire[ServiceAlias](NewService)
		assert.NoError(t, err)
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &service{}, srv)
		srvAlias, err := di.New[ServiceAlias]()
		assert.NoError(t, err)
		assert.NotNil(t, srvAlias)
		assert.IsType(t, &service{}, srvAlias)
		assert.NotEqual(t, fmt.Sprintf("%p", srv), fmt.Sprintf("%p", srvAlias))
	})

	t.Run("Circular", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewServiceCircular)
		assert.NoError(t, err)
		err = di.Wire[ServiceChain](NewServiceChain)
		assert.NoError(t, err)
		_, err = di.New[ServiceChain]()
		assert.ErrorIs(t, err, di.ErrCircularDependencies)
	})

	t.Run("NoFallback", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.NoError(t, err)
		err = di.Wire[Service](NewServiceCircular)
		assert.NoError(t, err)
		err = di.Wire[ServiceChain](NewServiceChain)
		assert.NoError(t, err)
		_, err = di.New[ServiceChain]()
		assert.ErrorIs(t, err, di.ErrCircularDependencies)
	})

	t.Run("Fallback", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.NoError(t, err)
		err = di.Wire[Service](NewServiceCircular, di.Fallback())
		assert.NoError(t, err)
		err = di.Wire[ServiceChain](NewServiceChain)
		assert.NoError(t, err)
		srv, err := di.New[ServiceChain]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceChain{}, srv)
	})

	t.Run("NoErr", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewServiceNoErr)
		assert.NoError(t, err)
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &service{}, srv)
	})

	t.Run("WithErr", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewServiceWithErr)
		assert.NoError(t, err)
		_, err = di.New[Service]()
		assert.ErrorContains(t, err, "some error")
	})

	t.Run("NoReturn", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](func() {})
		assert.NoError(t, err)
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.Nil(t, srv)
	})

	t.Run("ErrorOnly", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](func() error { return nil })
		assert.NoError(t, err)
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.Nil(t, srv)
	})

	t.Run("NewFor", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.Nil(t, err)
		err = di.Wire[Service](NewServiceFor, di.For[ServiceChain]())
		assert.Nil(t, err)
		srv, err := di.New[Service](di.For[ServiceChain]())
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceFor{}, srv)
	})

	t.Run("NewMultyFor", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.Nil(t, err)
		err = di.Wire[Service](NewServiceFor, di.For[ServiceChain]())
		assert.NoError(t, err)
		_, err = di.New[Service](di.For[ServiceChain](), di.For[ServiceAlias]())
		assert.ErrorIs(t, err, di.ErrTooMuchTargets)
	})

	t.Run("For", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[Service](NewService)
		assert.Nil(t, err)
		err = di.Wire[Service](NewServiceFor, di.For[ServiceChain]())
		assert.Nil(t, err)
		err = di.Wire[ServiceChain](NewServiceChain)
		assert.Nil(t, err)
		srvChain, err := di.New[ServiceChain]()
		assert.NoError(t, err)
		assert.NotNil(t, srvChain)
		assert.IsType(t, &serviceChain{}, srvChain)
		assert.IsType(t, &serviceFor{}, srvChain.(*serviceChain).Service())
		srv, err := di.New[Service]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &service{}, srv)
	})

	t.Run("DefaultsWrongAddress", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(NewService, di.Alias[Service]())
		assert.NoError(t, err)
		err = di.Wire[ServiceDefaults](NewServiceDefaults, di.Defaults(map[int]any{
			0: 1,
			1: "7",
			2: NewService,
			3: di.Address("some key"),
			4: []bool{true, false},
		}))
		assert.NoError(t, err)
		_, err = di.New[ServiceDefaults]()
		assert.ErrorIs(t, err, di.ErrKeyNotFound)
	})

	t.Run("DefaultsWrongType", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(NewService, di.Alias[Service]())
		assert.NoError(t, err)
		err = di.Wire[ServiceDefaults](NewServiceDefaults, di.Defaults(map[int]any{
			0: "1",
			1: "7",
			2: NewService,
			3: NewService,
			4: []bool{true, false},
		}))
		assert.NoError(t, err)
		_, err = di.New[ServiceDefaults]()
		assert.ErrorIs(t, err, di.ErrMismatchedTypes)
	})

	t.Run("DefaultsWrongInterface", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(NewService, di.Alias[Service]())
		assert.NoError(t, err)
		err = di.Wire[ServiceDefaults](NewServiceDefaults, di.Defaults(map[int]any{
			2: func() *serviceChain {
				return &serviceChain{}
			},
		}))
		assert.NoError(t, err)
		_, err = di.New[ServiceDefaults]()
		assert.ErrorIs(t, err, di.ErrMismatchedTypes)
	})

	t.Run("DefaultsNilValue", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(NewService, di.Alias[Service]())
		assert.NoError(t, err)
		err = di.Wire[ServiceDefaults](NewServiceDefaults, di.Defaults(map[int]any{
			0: 1,
			1: "7",
			2: NewService(),
			3: NewService,
			4: nil,
		}))
		assert.NoError(t, err)
		srv, err := di.New[ServiceDefaults]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceDefaults{}, srv)
		assert.Equal(t, []bool{}, srv.(*serviceDefaults).V)
		assert.IsType(t, &service{}, srv.(*serviceDefaults).Wired)
		assert.IsType(t, &service{}, srv.(*serviceDefaults).srv)
	})

	t.Run("DefaultsMissedValues", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		_, err := di.Define(NewService, di.Alias[Service]())
		assert.NoError(t, err)
		err = di.Wire[ServiceDefaults](NewServiceDefaults)
		assert.NoError(t, err)
		srv, err := di.New[ServiceDefaults]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceDefaults{}, srv)
		assert.Equal(t, 0, srv.(*serviceDefaults).A)
		assert.Equal(t, "", srv.(*serviceDefaults).B)
		assert.IsType(t, &service{}, srv.(*serviceDefaults).Wired)
		assert.IsType(t, &service{}, srv.(*serviceDefaults).srv)
		assert.Equal(t, []bool{}, srv.(*serviceDefaults).V)
	})

	t.Run("Defaults", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		addr, err := di.Define(NewService, di.Alias[Service]())
		assert.NoError(t, err)
		customSrv := NewService()
		assert.NotNil(t, customSrv)
		err = di.Wire[ServiceDefaults](NewServiceDefaults, di.Defaults(map[int]any{
			0: 1,
			1: "7",
			2: customSrv,
			3: addr,
			4: []bool{true, false},
		}))
		assert.NoError(t, err)
		srv, err := di.New[ServiceDefaults]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceDefaults{}, srv)
		assert.Equal(t, 1, srv.(*serviceDefaults).A)
		assert.Equal(t, "7", srv.(*serviceDefaults).B)
		assert.IsType(t, &service{}, srv.(*serviceDefaults).Wired)
		assert.Equal(t, fmt.Sprintf("%p", customSrv), fmt.Sprintf("%p", srv.(*serviceDefaults).srv))
		assert.IsType(t, &service{}, srv.(*serviceDefaults).srv)
		assert.NotEqual(t, fmt.Sprintf("%p", srv.(*serviceDefaults).srv), fmt.Sprintf("%p", srv.(*serviceDefaults).Wired))
		assert.Equal(t, []bool{true, false}, srv.(*serviceDefaults).V)
	})

	t.Run("WithCloser", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[ServiceCloser](NewServiceCloser)
		assert.NoError(t, err)
		err = di.Wire[ServiceChainCloser](NewServiceCloserChain)
		assert.NoError(t, err)
		srv, err := di.New[ServiceChainCloser]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.IsType(t, &serviceChainCloser{}, srv.(*serviceChainCloser))
		srvContainer, closer, err := di.DefaultContainer.New(di.TypeFor[ServiceChainCloser](), di.WithCloser())
		assert.NoError(t, err)
		assert.NotNil(t, srvContainer)
		assert.IsType(t, &serviceChainCloser{}, srvContainer.(*serviceChainCloser))
		assert.Equal(t, fmt.Sprintf("%p", srv), fmt.Sprintf("%p", srvContainer))

		var (
			firstClosed  bool
			secondClosed bool
		)
		closerHandler = func() error {
			assert.True(t, firstClosed)
			secondClosed = true
			return nil
		}
		closerChainHandler = func() error {
			assert.False(t, secondClosed)
			firstClosed = true
			return nil
		}
		assert.False(t, firstClosed)
		assert.False(t, secondClosed)
		err = closer()
		assert.NoError(t, err)
		assert.True(t, firstClosed)
		assert.True(t, secondClosed)
	})

	t.Run("NewWithCloser", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[ServiceCloser](NewServiceCloser)
		assert.NoError(t, err)
		err = di.Wire[ServiceChainCloser](NewServiceCloserChain)
		assert.NoError(t, err)
		err = di.Wire[ServiceWrapperCloser](NewServiceWrapperCloser)
		assert.NoError(t, err)
		srv, closer, err := di.NewWithCloser[ServiceWrapperCloser]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.NotNil(t, closer)

		var (
			firstClosed  bool
			secondClosed bool
			thirdClosed  bool
		)
		closerHandler = func() error {
			panic("should not be called")
		}
		closerChainHandler = func() error {
			panic("should not be called")
		}
		closerWrapperHandler = func() error {
			assert.False(t, firstClosed)
			assert.False(t, secondClosed)
			assert.False(t, thirdClosed)
			firstClosed = true
			return errors.New("oops")
		}
		assert.False(t, firstClosed)
		assert.False(t, secondClosed)
		assert.False(t, thirdClosed)
		err = closer()
		assert.ErrorIs(t, err, di.ErrCloseFailed)
		assert.True(t, firstClosed)
		assert.False(t, secondClosed)
		assert.False(t, thirdClosed)
	})

	t.Run("NewWithCloserPanics", func(t *testing.T) {
		di.DefaultContainer = di.NewContainer()
		err := di.Wire[ServiceCloser](NewServiceCloser)
		assert.NoError(t, err)
		err = di.Wire[ServiceChainCloser](NewServiceCloserChain)
		assert.NoError(t, err)
		err = di.Wire[ServiceWrapperCloser](NewServiceWrapperCloser)
		assert.NoError(t, err)
		srv, closer, err := di.NewWithCloser[ServiceWrapperCloser]()
		assert.NoError(t, err)
		assert.NotNil(t, srv)
		assert.NotNil(t, closer)

		var (
			firstClosed  bool
			secondClosed bool
			thirdClosed  bool
		)
		closerHandler = func() error {
			panic("should not be called")
		}
		closerChainHandler = func() error {
			panic("should not be called")
		}
		closerWrapperHandler = func() error {
			firstClosed = true
			panic("oops")
		}
		assert.False(t, firstClosed)
		assert.False(t, secondClosed)
		assert.False(t, thirdClosed)
		err = closer()
		assert.ErrorIs(t, err, di.ErrCloseFailed)
		assert.True(t, firstClosed)
		assert.False(t, secondClosed)
		assert.False(t, thirdClosed)
	})

	t.Run("Tags", func(t *testing.T) {
		t.Run("Success", func(t *testing.T) {
			di.DefaultContainer = di.NewContainer()
			err := di.Wire[TagInterfaceOne](NewTagStructOne, di.Tag(TagKey))
			assert.NoError(t, err)
			_, err = di.Define(NewTagStructTwo, di.Alias[TagInterfaceTwo](), di.Tag(TagKey), di.Tag(TagKey), di.Tag(TagTwo))
			assert.NoError(t, err)
			err = di.Wire[TagsWrapper](NewWrapper, di.Defaults(map[int]any{
				0: di.Tags[TagInterface](TagKey),
			}))
			assert.NoError(t, err)
			srv, err := di.New[TagsWrapper]()
			assert.NoError(t, err)
			assert.NotNil(t, srv)
			assert.Len(t, srv.(*tagsWrapper).tags, 2)
		})

		t.Run("Fail", func(t *testing.T) {
			di.DefaultContainer = di.NewContainer()
			err := di.Wire[TagInterfaceOne](NewTagStructOne, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagInterfaceTwo](NewTagStructTwo, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagInterfaceThree](NewTagStructThree, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagsWrapper](NewWrapper, di.Defaults(map[int]any{
				0: di.Tags[TagInterface](TagKey),
			}))
			assert.Nil(t, err)
			_, err = di.New[TagsWrapper]()
			assert.ErrorContains(t, err, "oops")
		})

		t.Run("FailNonSlice", func(t *testing.T) {
			di.DefaultContainer = di.NewContainer()
			err := di.Wire[TagInterfaceOne](NewTagStructOne, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagInterfaceTwo](NewTagStructTwo, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagsWrapper](NewWrongWrapper, di.Defaults(map[int]any{
				0: di.Tags[TagInterface](TagKey),
			}))
			assert.Nil(t, err)
			_, err = di.New[TagsWrapper]()
			assert.ErrorIs(t, err, di.ErrTagsNoSlice)
		})

		t.Run("FailWrongType", func(t *testing.T) {
			di.DefaultContainer = di.NewContainer()
			err := di.Wire[TagInterfaceFour](NewTagStructFour, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagsWrapper](NewWrapper, di.Defaults(map[int]any{
				0: di.Tags[TagInterface](TagKey),
			}))
			assert.Nil(t, err)
			_, err = di.New[TagsWrapper]()
			assert.ErrorIs(t, err, di.ErrMismatchedTypes)
		})

		t.Run("FailWrongSlice", func(t *testing.T) {
			di.DefaultContainer = di.NewContainer()
			err := di.Wire[TagInterfaceOne](NewTagStructOne, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagInterfaceTwo](NewTagStructTwo, di.Tag(TagKey))
			assert.Nil(t, err)
			err = di.Wire[TagsWrapper](NewWrongSliceWrapper, di.Defaults(map[int]any{
				0: di.Tags[TagInterface](TagKey),
			}))
			assert.Nil(t, err)
			_, err = di.New[TagsWrapper]()
			assert.ErrorIs(t, err, di.ErrTagsNoSlice)
		})
	})
}
