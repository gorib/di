package di

type defineOption interface{ define() }

type wireOption interface{ wire() }

type newOption interface{ new() }

type optionHandler func(c *request) error

type option interface {
	handler() optionHandler
}

type baseOption struct {
	h optionHandler
}

func (b *baseOption) handler() optionHandler {
	return b.h
}

type defaultOption struct {
	wireOption
	defineOption
	*baseOption
}

func Defaults(defaults map[int]any) *defaultOption {
	return &defaultOption{baseOption: &baseOption{func(c *request) error {
		c.defaults = defaults
		return nil
	}}}
}

type aliasOption struct {
	defineOption
	*baseOption
}

func Alias[Interface any]() *aliasOption {
	return aliasOf(TypeFor[Interface]())
}

func aliasOf(alias *Type) *aliasOption {
	return &aliasOption{baseOption: &baseOption{func(c *request) error {
		c.aliases = append(c.aliases, alias)
		return nil
	}}}
}

type forOption struct {
	wireOption
	defineOption
	newOption
	*baseOption
}

func For[Interface any]() *forOption {
	return forOf(TypeFor[Interface]())
}

func forOf(f *Type) *forOption {
	return &forOption{baseOption: &baseOption{func(c *request) error {
		c.targets = append(c.targets, f)
		return nil
	}}}
}

type tagOption struct {
	wireOption
	defineOption
	*baseOption
}

func Tag(tag any) *tagOption {
	return &tagOption{baseOption: &baseOption{func(c *request) error {
		c.tags = append(c.tags, tag)
		return nil
	}}}
}

type fallbackOption struct {
	wireOption
	defineOption
	*baseOption
}

func Fallback() *fallbackOption {
	return &fallbackOption{baseOption: &baseOption{func(c *request) error {
		c.fallback = true
		return nil
	}}}
}

type closerOption struct {
	newOption
	*baseOption
}

func WithCloser() *closerOption {
	return &closerOption{baseOption: &baseOption{func(c *request) error {
		return nil
	}}}
}
