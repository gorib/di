package di

type Address string

type tagsHandler func() ([]*entity, error)

var DefaultContainer = NewContainer()

func MustDefine(constructor any, options ...defineOption) Address {
	instanceKey, err := Define(constructor, options...)
	if err != nil {
		panic(err)
	}
	return instanceKey
}

func Define(constructor any, opts ...defineOption) (Address, error) {
	return DefaultContainer.Define(constructor, opts...)
}

func MustWire[Interface any](constructor any, options ...wireOption) {
	err := Wire[Interface](constructor, options...)
	if err != nil {
		panic(err)
	}
}

func Wire[Interface any](constructor any, options ...wireOption) error {
	return DefaultContainer.Wire(constructor, TypeFor[Interface](), options...)
}

func New[Interface any](opts ...newOption) (Interface, error) {
	i, _, err := DefaultContainer.New(TypeFor[Interface](), opts...)
	if err != nil {
		return *new(Interface), err
	}
	if i == nil {
		return *new(Interface), nil
	}
	return i.(Interface), nil
}

func NewWithCloser[Interface any](opts ...newOption) (Interface, func() error, error) {
	i, closer, err := DefaultContainer.New(TypeFor[Interface](), append([]newOption{WithCloser()}, opts...)...)
	if err != nil {
		return *new(Interface), nil, err
	}
	if i == nil {
		return *new(Interface), closer, nil
	}
	return i.(Interface), closer, nil
}

func Tags[Interface any](label any) tagsHandler {
	return DefaultContainer.Tags(label)
}
