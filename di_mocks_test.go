package di_test

import (
	"fmt"
)

// common

type Service interface{ _srv() }

type ServiceAlias interface{ _srv() }

type ServiceChain interface{ _chain() }

func NewService() *service {
	return &service{}
}

func NewServiceCircular(srv ServiceChain) *service {
	return &service{}
}

func NewServiceNoErr() (*service, error) {
	return &service{}, nil
}

func NewServiceWithErr() (*service, error) {
	return nil, fmt.Errorf("some error")
}

func NewServiceFor() *serviceFor {
	return &serviceFor{}
}

type service struct{ Service }

type serviceFor struct{ Service }

func NewServiceChain(srv Service) ServiceChain {
	return &serviceChain{
		srv: srv,
	}
}

type serviceChain struct {
	ServiceChain
	srv Service
}

func (s *serviceChain) Service() Service {
	return s.srv
}

// defaults

type ServiceDefaults interface{ _defaults() }

func NewServiceDefaults(a int, b string, srv, srvWire Service, v ...bool) *serviceDefaults {
	return &serviceDefaults{
		srv:   srv,
		Wired: srvWire,
		A:     a,
		B:     b,
		V:     v,
	}
}

type serviceDefaults struct {
	ServiceDefaults
	srv   Service
	Wired Service
	A     int
	B     string
	V     []bool
}

// closer

type ServiceCloser interface{ _closer() }

type ServiceChainCloser interface{ _chainCloser() }

type ServiceWrapperCloser interface{ _wrapperCloser() }

func NewServiceCloser() ServiceCloser {
	return &serviceCloser{}
}

type serviceCloser struct {
	ServiceCloser
}

var closerHandler func() error

func (s *serviceCloser) Close() error {
	return closerHandler()
}

func NewServiceCloserChain(srv ServiceCloser) ServiceChainCloser {
	return &serviceChainCloser{
		srv: srv,
	}
}

type serviceChainCloser struct {
	ServiceChainCloser
	srv ServiceCloser
}

var closerChainHandler func() error

func (s *serviceChainCloser) Close() error {
	return closerChainHandler()
}

func NewServiceWrapperCloser(srv ServiceCloser, chain ServiceChainCloser) ServiceWrapperCloser {
	return &serviceWrapperCloser{
		srv:   srv,
		chain: chain,
	}
}

type serviceWrapperCloser struct {
	ServiceWrapperCloser
	srv   ServiceCloser
	chain ServiceChainCloser
}

var closerWrapperHandler func() error

func (s *serviceWrapperCloser) Close() error {
	return closerWrapperHandler()
}

// tags

type (
	tagKey struct{}
	tagTwo struct{}
)

var (
	TagKey tagKey
	TagTwo tagTwo
)

type TagInterface interface{ _tag() }

type TagInterfaceOne interface{ TagInterface }

type TagInterfaceTwo interface{ TagInterface }

type TagInterfaceThree interface{ TagInterface }

type TagInterfaceFour interface{ _tagFour() }

type TagsWrapper interface{ _tagWrapper() }

func NewTagStructOne() *tagStructOne {
	return &tagStructOne{}
}

type tagStructOne struct{ TagInterfaceOne }

func NewTagStructTwo() (*tagStructTwo, error) {
	return &tagStructTwo{}, nil
}

type tagStructTwo struct{ TagInterfaceTwo }

func NewTagStructThree() (*tagStructThree, error) {
	return nil, fmt.Errorf("oops")
}

func NewTagStructFour() (*tagStructFour, error) {
	return &tagStructFour{}, nil
}

type tagStructFour struct{ TagInterfaceFour }

type tagStructThree struct{ TagInterfaceThree }

func NewWrapper(tags []TagInterface) TagsWrapper {
	return &tagsWrapper{tags: tags}
}

func NewWrongWrapper(tags int) TagsWrapper {
	return &tagsWrapper{}
}

func NewWrongSliceWrapper(tags []int) TagsWrapper {
	return &tagsWrapper{}
}

type tagsWrapper struct {
	TagsWrapper
	tags []TagInterface
}
