package di

import (
	"crypto/sha1"
	"encoding/hex"
	"reflect"
)

func typeOf(v any) *Type {
	switch t := v.(type) {
	case reflect.Type:
		return &Type{
			Type: t,
		}
	case *Type:
		return t
	default:
		return &Type{
			Type: reflect.TypeOf(v),
		}
	}
}

func TypeFor[Interface any]() *Type {
	return typeOf(reflect.TypeFor[Interface]())
}

type Type struct {
	reflect.Type
	deref *Type
}

func (m *Type) Name() Address {
	if m == nil || m.Type == nil {
		return ""
	}
	return Address(m.Type.PkgPath() + "." + m.Type.Name())
}

func (m *Type) In(idx int) *Type {
	return typeOf(m.Type.In(idx))
}

func (m *Type) Out(idx int) *Type {
	return typeOf(m.Type.Out(idx))
}

func (m *Type) Elem() *Type {
	return typeOf(m.Type.Elem())
}

func (m *Type) Deref() *Type {
	if m.deref == nil {
		for m.deref = typeOf(m.Type); m.deref.Kind() == reflect.Ptr; m.deref = m.deref.Elem() {
		}
	}
	return m.deref
}

func encrypt(need, target Address) string {
	digest := sha1.New()
	digest.Write([]byte(need + "::" + target))
	return hex.EncodeToString(digest.Sum(nil))
}
