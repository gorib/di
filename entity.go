package di

import "reflect"

type entity struct {
	constructor any
	defaults    map[int]any
	_f          *Type
	_i          *reflect.Value
	graph       []*entity
}

func (e *entity) Initialize(instance *reflect.Value) {
	e._i = instance
}

type request struct {
	constructor any
	defaults    map[int]any
	tags        []any
	targets     []*Type
	aliases     []*Type
	fallback    bool

	_f *Type
}

func (r *request) f() *Type {
	if r._f == nil {
		r._f = typeOf(r.constructor)
	}
	return r._f
}

func (r *request) entity() *entity {
	return &entity{
		constructor: r.constructor,
		defaults:    r.defaults,
		_f:          r.f(),
	}
}

type tag struct {
	address Address
	iface   *Type
}
